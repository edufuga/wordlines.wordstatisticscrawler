package com.gitlab.edufuga.wordlines.wordstatisticscrawler

import groovy.transform.TypeChecked

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.stream.Collectors
import java.util.stream.Stream

import static java.util.stream.Collectors.groupingBy

@TypeChecked
class WordStatisticsCrawler {
    private Path root

    WordStatisticsCrawler(Path root) {
        this.root = root
    }

    List<String> getTableOfRecords() {
        List<WordStatusDate> currentRecords = getCurrentRecords()

        List<String> table = new ArrayList<>()
        for (WordStatusDate record : currentRecords) {

            String dayString = WordStatusDate.dayFormat.format(record.getDay())
            String result = "$dayString\t$record.word\t$record.status".toString()
            table.add(result)
        }

        return table
    }

    void printLaTeXOfRecords() {
        println """\\documentclass[a5paper, 10pt]{article}

\\usepackage[a5paper]{geometry}
\\usepackage{lmodern}
\\usepackage{longtable}

\\begin{document}
\\begin{longtable}{p{.34\\textwidth} p{.33\\textwidth} p{.33\\textwidth}}
\\textbf{Date} & \\textbf{Word} & \\textbf{Status} \\\\"""

        for (WordStatusDate record : getCurrentRecords()) {
            def latexFormat = new SimpleDateFormat("yyyy--MM--dd");
            String dayString = latexFormat.format(record.getDay())
            println "$dayString & $record.word & \\texttt{$record.status} \\\\".toString()
        }

    println """\\end{longtable}
\\end{document}
"""
    }

    void printNanoSyntaxHighlighting() {
        String language = root.last().toString()

        Map<String, List<WordStatusDate>> recordsByState = new HashMap<>()
        for (WordStatusDate record : getCurrentRecords()) {
            recordsByState.putIfAbsent(record.getStatus(), new ArrayList<WordStatusDate>())
            recordsByState.get(record.getStatus()).add(record)
        }

        println """syntax "${language.toUpperCase()}" "\\.${language.toLowerCase()}\$"
comment "//"
"""
        for (Map.Entry<String, List<WordStatusDate>> entries : recordsByState.entrySet()) {
            String status = entries.getKey()
            String color = "nothing"
            switch (status) {
                case "UNKNOWN": color = "red"; break;
                case "GUESSED": color = "yellow"; break;
                case "COMPREHENDED": color = "magenta"; break;
                case "KNOWN": color = "blue"; break;
                case "IGNORED": color = "green"; break;
                default: continue;
            }
            String separator = "|"
            List<WordStatusDate> words = entries.getValue()
            if (words==null || words.isEmpty())
                continue

            String pipeSeparatedWords = entries.getValue()
                    .findAll(record -> record != null && !record.getWord().isEmpty())
                    .collect { record ->
                        String word = record.getWord()
                        if (word[0].toUpperCase() != word[0]) {
                            String recordFirstUpperCase = word[0].toUpperCase() + ((word.size() > 1) ? word.substring(1) : "")
                            return word + separator + recordFirstUpperCase
                        }
                        else {
                            return word
                        }
                    }
                    .join(separator)
            println "color ${color} \"\\<($pipeSeparatedWords)\\>\""
        }
    }

    void printSublimeHightlighting() {
        String language = root.last().toString()

        Map<String, List<WordStatusDate>> recordsByState = new HashMap<>()
        for (WordStatusDate record : getCurrentRecords()) {
            recordsByState.putIfAbsent(record.getStatus(), new ArrayList<WordStatusDate>())
            recordsByState.get(record.getStatus()).add(record)
        }

        println """%YAML 1.2
---
# See http://www.sublimetext.com/docs/3/syntax.html
file_extensions:
  - ${language.toLowerCase()}
scope: source.example-c

contexts:
  # The prototype context is prepended to all contexts but those setting
  # meta_include_prototype: false.
  prototype:
    - include: comments

  main:
    # The main context is the initial starting point of our syntax.
    # Include other contexts from here (or specify them directly).
    - include: known
    - include: comprehended
    - include: guessed
    - include: unknown
    - include: ignored
    - include: numbers
    - include: strings
"""

        for (Map.Entry<String, List<WordStatusDate>> entries : recordsByState.entrySet()) {
            String status = entries.getKey()
            String color = "nothing"
            switch (status) {
                case "UNKNOWN": color = "red"; break;
                case "GUESSED": color = "yellow"; break;
                case "COMPREHENDED": color = "magenta"; break;
                case "KNOWN": color = "blue"; break;
                case "IGNORED": color = "green"; break;
                default: continue;
            }
            String separator = "|"
            List<WordStatusDate> words = entries.getValue()
            if (words==null || words.isEmpty())
                continue

            String pipeSeparatedWords = entries.getValue()
                    .findAll(record -> record != null && !record.getWord().isEmpty())
                    .collect { record ->
                        String word = record.getWord()
                        if (word[0].toUpperCase() != word[0]) {
                            String recordFirstUpperCase = word[0].toUpperCase() + ((word.size() > 1) ? word.substring(1) : "")
                            return word + separator + recordFirstUpperCase
                        }
                        else {
                            return word
                        }
                    }
                    .join(separator)
            println """
  ${status.toLowerCase()}:
    - match: '\\b($pipeSeparatedWords)\\b'
      scope: keyword.control.${status.toLowerCase()}
"""
        }

println """
  numbers:
    - match: '\\b(-)?[0-9.]+\\b'
      scope: constant.numeric.example-c

  strings:
    # Strings begin and end with quotes, and use backslashes as an escape
    # character.
    - match: '"'
      scope: punctuation.definition.string.begin.example-c
      push: inside_string

  inside_string:
    - meta_include_prototype: false
    - meta_scope: string.quoted.double.example-c
    - match: '\\.'
      scope: constant.character.escape.example-c
    - match: '"'
      scope: punctuation.definition.string.end.example-c
      pop: true

  comments:
    # Comments begin with a '//' and finish at the end of the line.
    - match: '//'
      scope: punctuation.definition.comment.example-c
      push:
        # This is an anonymous context push for brevity.
        - meta_scope: comment.line.double-slash.example-c
        - match: \$\\n?
          pop: true
"""
    }

    void crawl() {
        List<Path> allPaths = filewalk(root)

        List<WordStatusDate> allRecords = new ArrayList<>()
        for (Path path : allPaths) {
            WordStatusDate record = readRecord(path)
            allRecords.add(record)
        }

        println "Number of records: " + allRecords.size()
        println ""

        // Sort all records in chronological order.
        allRecords.sort()

        /*
        for (WordStatusDate record : allRecords) {
            println record
        }
         */

        Map<Date, List<WordStatusDate>> allRecordsByDay = allRecords.stream()
                .collect(groupingBy(WordStatusDate::getDay,() -> new TreeMap<>(), Collectors.toList()))

        Map<Date, Integer> numbersByDay = new TreeMap<>()

        int sum = 0
        for (Map.Entry<Date, List<WordStatusDate>> entry : allRecordsByDay.entrySet()) {
            Date day = entry.getKey()
            List<WordStatusDate> recordsAtDay = entry.getValue()
            recordsAtDay.sort()

            numbersByDay.put(day, recordsAtDay.size())

            //println "${WordStatusDate.dayFormat.format(day)}\t${recordsAtDay.size()}"
            sum = sum + recordsAtDay.size()
        }

        /*
        println ""
        println sum
         */

        /*
        println ""

        for (Map.Entry<Date, List<WordStatusDate>> entry : allRecordsByDay.entrySet()) {
            Date day = entry.getKey()
            List<WordStatusDate> recordsAtDay = entry.getValue()

            String dayString = WordStatusDate.dayFormat.format(day)

            println dayString
            println "-" * dayString.size()
            for (WordStatusDate record : recordsAtDay) {
                //println "$record.word\t$record.status\t${WordStatusDate.format.format(record.date)}"
                println "$record.word\t$record.status"
            }

            println ""
        }
        println ""
         */

        // Day -> Status -> Word
        Map<Date, Map<String, List<String>>> wordsGroupedByStatusGroupedByDay = new TreeMap<>()

        // Status -> Day -> Word
        // Attention: This data structure may not be so relevant. But I want to have it splitted by status...
        Map<String, Map<Date, List<String>>> wordsGroupedByDayGroupedByStatus = new TreeMap<>()

        Set<String> allStates = new HashSet<>()
        Set<Date> allDates = new TreeSet<>()

        // Make some sort of "daily glossary", more condensed (grouping also by status)
        for (Map.Entry<Date, List<WordStatusDate>> entry : allRecordsByDay.entrySet()) {
            Date day = entry.getKey()
            allDates.add(day) // This is relevant for the further analysis

            List<WordStatusDate> recordsAtDay = entry.getValue()

            String dayString = WordStatusDate.dayFormat.format(day)
            println dayString
            println "-" * dayString.size()
            println ""

            Map<String, List<WordStatusDate>> recordsAtDayByStatus = recordsAtDay.stream().collect(groupingBy(WordStatusDate::getStatus))
            for (Map.Entry<String, List<WordStatusDate>> e : recordsAtDayByStatus.entrySet()) {
                String status = e.getKey()
                allStates.add(status) // This is relevant for the further analysis.
                List<WordStatusDate> recordAtDayWithGivenStatus = e.getValue()
                List<String> words = recordAtDayWithGivenStatus.stream().map(WordStatusDate::getWord).collect(Collectors.toList())
                words.sort()

                println status
                println words.join(", ")
                println ""

                // transform data structure (Day -> Status -> Word)
                if (!wordsGroupedByStatusGroupedByDay.containsKey(day)) {
                    wordsGroupedByStatusGroupedByDay.put(day, new TreeMap<String, List<String>>())
                }

                Map<String, List<String>> wordsGroupedByStatusAtGivenDay = wordsGroupedByStatusGroupedByDay.get(day)
                for (WordStatusDate record : recordAtDayWithGivenStatus) {
                    if (!wordsGroupedByStatusAtGivenDay.containsKey(status)) {
                        wordsGroupedByStatusAtGivenDay.put(status, new ArrayList<>())
                    }
                    List<String> wordss = wordsGroupedByStatusAtGivenDay.get(status)
                    wordss.add(record.word)
                }

                // Attention: This data structure may not be so relevant. But I want to have it splitted by status...
                // Transform data structure (Status -> Day -> Word)
                if (!wordsGroupedByDayGroupedByStatus.containsKey(status)) {
                    wordsGroupedByDayGroupedByStatus.put(status, new TreeMap<Date, List<String>>())
                }

                Map<Date, List<String>> wordsGroupedByDayWithGivenStatus = wordsGroupedByDayGroupedByStatus.get(status)
                for (WordStatusDate record : recordAtDayWithGivenStatus) {
                    if (!wordsGroupedByDayWithGivenStatus.containsKey(day)) {
                        wordsGroupedByDayWithGivenStatus.put(day, new ArrayList<>())
                    }
                    List<String> wordss = wordsGroupedByDayWithGivenStatus.get(day)
                    wordss.add(record.word)
                }
            }
            println ""

            //wordsGroupedByStatusGroupedByDay.each {println it}
            //println ""

            //wordsGroupedByDayGroupedByStatus.each {println it}
            //println ""
        }

        println "All states: $allStates"
        println ""

        println "Counts by states"
        println ""

        print "Date"
        print "\t"
        for (String state : allStates) {
            print state
            print "\t"
        }
        println ""

        for (Date date : allDates) {
            print WordStatusDate.dayFormat.format(date)
            print "\t"
            for (String state : allStates) {
                Map<Date, List<String>> wordsGroupedByDayWithGivenState = wordsGroupedByDayGroupedByStatus.get(state)
                if (wordsGroupedByDayWithGivenState) {
                    List<String> wordsWithGivenStateAndDate = wordsGroupedByDayWithGivenState.get(date)
                    if (wordsWithGivenStateAndDate) {
                        int count = wordsWithGivenStateAndDate.size()

                        print count
                        print "\t"
                    }
                    else {
                        print 0
                        print "\t"
                    }
                }

            }
            println ""
        }

        // Total count by states...

        println ""
        println "Total count by states"
        println ""

        print "Date"
        print "\t"
        for (String state : allStates) {
            print state
            print "\t"
        }
        println ""

        Map<String, Integer> totalCountsByState = new HashMap<>()
        for (Date date : allDates) {
            print WordStatusDate.dayFormat.format(date)
            print "\t"
            for (String state : allStates) {
                totalCountsByState.putIfAbsent(state, 0 as Integer)

                Map<Date, List<String>> wordsGroupedByDayWithGivenState = wordsGroupedByDayGroupedByStatus.get(state)
                if (wordsGroupedByDayWithGivenState) {
                    List<String> wordsWithGivenStateAndDate = wordsGroupedByDayWithGivenState.get(date)
                    if (wordsWithGivenStateAndDate) {
                        Integer count = wordsWithGivenStateAndDate.size()

                        Integer totalCount = count + totalCountsByState.get(state)
                        totalCountsByState.put(state, totalCount)
                        print totalCount
                        print "\t"
                    }
                    else {
                        print totalCountsByState.get(state)
                        print "\t"
                    }
                }

            }
            println ""
        }

        // Evolution of words (All records that contain the same word)
        Map<String, List<WordStatusDate>> evolutionOfWords = new HashMap<>()
        for (WordStatusDate record : allRecords) {
            String word = record.getWord()
            evolutionOfWords.putIfAbsent(word, new ArrayList<>())
            evolutionOfWords.get(word).add(record)
        }

        // Current records (Only the last record that contain a given word)
        // NOTE: Actually this should be the SAME as the contents of the files outside the "history" folder...
        List<WordStatusDate> currentRecordsList = new ArrayList<>()
        Map<String, WordStatusDate> currentRecords = new HashMap<>()
        for (Map.Entry<String, List<WordStatusDate>> entry : evolutionOfWords.entrySet()) {
            String word = entry.getKey()
            List<WordStatusDate> records = entry.getValue()
            records.sort()
            WordStatusDate currentRecord = records.last()
            currentRecords.put(word, currentRecord)
            currentRecordsList.add(currentRecord)
        }

        currentRecordsList.sort()

        /*
        println ""
        println "Current records"
        println ""
        for (WordStatusDate record : currentRecordsList) {
            //println record
            println "${WordStatusDate.dayFormat.format(record.date)}\t$record.word\t$record.status"
        }

         */

        // Evolution of words up to the current day (Only the words that were added and reviewed previously)
        TreeMap<Date, List<WordStatusDate>> wordsUpToTheCurrentDays = new TreeMap<>()
        for (Date date : allDates) {
            wordsUpToTheCurrentDays.put(date, new ArrayList<>())
        }
        for (WordStatusDate record : allRecords) {
            Date recordDay = record.getDay()

            for (Map.Entry<Date, List<WordStatusDate>> mapEntry : wordsUpToTheCurrentDays)
            {
                Date mapDate = mapEntry.getKey()
                List<WordStatusDate> mapRecords = mapEntry.getValue()

                if (recordDay <= mapDate) {
                    mapRecords.add (record)
                }
            }
        }

        /*
        println ""
        println "Word evolution by date" // History of the the vocabulary
        for (Map.Entry<Date, List<WordStatusDate>> mapEntry : wordsUpToTheCurrentDays) {
            Date day = mapEntry.getKey()
 
            String dayString = WordStatusDate.dayFormat.format(day)
            println dayString
            println "-" * dayString.size()

            List<WordStatusDate> mapRecords = mapEntry.getValue()
            // mapRecords.sort()
            for (WordStatusDate record : mapRecords) {
                println "${WordStatusDate.dayFormat.format(record.date)}\t$record.word\t$record.status"
            }
            println ""
        }
         */

        // History grouped by word (the evolution of every word until the given day)
        TreeMap<Date, TreeMap<String, List<WordStatusDate>>> historyGroupedByWord = new TreeMap<>()
        for (Map.Entry<Date, List<WordStatusDate>> mapEntry : wordsUpToTheCurrentDays) {
            Date day = mapEntry.getKey()
            List<WordStatusDate> records = mapEntry.getValue()
            TreeMap<String, List<WordStatusDate>> recordsGroupedByWord = records.stream()
                    .collect(groupingBy(WordStatusDate::getWord, () -> new TreeMap<>(), Collectors.toList()))
            historyGroupedByWord.put(day, recordsGroupedByWord)
        }

        println ""
        println "Word evolution by date (grouping words)"
        println ""

        for (Map.Entry<Date, TreeMap<String, List<WordStatusDate>>> mapEntry : historyGroupedByWord) {
            Date day = mapEntry.getKey()

            // I am actually only interested in the *last* entry, i.e. the complete evolution of the vocabulary.
            if (day < allDates.last()) {
                continue
            }

            String dayString = WordStatusDate.dayFormat.format(day)
            println dayString
            println "-" * dayString.size()

            TreeMap<String, List<WordStatusDate>> groupedRecordsForWord = mapEntry.getValue()
            for (Map.Entry<String, List<WordStatusDate>> groupedEntry : groupedRecordsForWord.entrySet()) {
                String word = groupedEntry.getKey()
                List<WordStatusDate> recordsForWord = groupedEntry.getValue()
                println "$word: ${recordsForWord.collect {"(${WordStatusDate.dayFormat.format(it.date)}, $it.status)"}.join(" -> ")}"
            }
            println ""
        }

        // Evolution by states (how many words of every state exist up to a given date?)
        println "Evolution by states"
        println ""

        print "Date"
        print "\t"
        for (String state : allStates) {
            print state
            print "\t"
        }
        println ""

        for (Date date : allDates) {
            print WordStatusDate.dayFormat.format(date)
            print "\t"

            // Fill "day evolution": Only the most current record for the given word.
            TreeMap<String, WordStatusDate> dayEvolution = new TreeMap<>()
            TreeMap<String, List<WordStatusDate>> dayHistory = historyGroupedByWord.get(date)
            for (Map.Entry<String, List<WordStatusDate>> dayEvolutionEntry : dayHistory.entrySet()) {
                String word = dayEvolutionEntry.getKey()
                List<WordStatusDate> oldAndCurrentRecords = dayEvolutionEntry.getValue()
                WordStatusDate currentRecord = oldAndCurrentRecords.last()
                dayEvolution.put(word, currentRecord)
            }

            // Split the day evolution according to the different states.
            Collection<WordStatusDate> flatDayEvolution = dayEvolution.values()

            for (String state : allStates) {
                Collection<WordStatusDate> recordsWithGivenStatus = flatDayEvolution.findAll {state.equals(it.getStatus()) }
                print recordsWithGivenStatus.size()
                print "\t"
            }
            println ""
        }
    }

    private static List<Path> filewalk(Path root) {
        List<Path> result
        try (Stream<Path> walk = Files.walk(root)) {
            result = walk.filter(Files::isRegularFile)
                    .collect(Collectors.toList())
        }
        return result
    }

    private static List<Path> getCurrentPaths(Path root) {
        List<Path> result
        try (Stream<Path> children = Files.list(root)) {
            result = children.filter(Files::isRegularFile)
                    .collect(Collectors.toList())
        }
        return result
    }

    private static WordStatusDate readRecord(Path path) {
        List<String> lines = Files.readAllLines(path)
        String firstLine = lines.get(0)

        List<String> splittedLine = firstLine.split("\t").toList()

        String word = splittedLine.get(0)
        String status = splittedLine.get(1)
        String date = splittedLine.get(2)

        return new WordStatusDate(word, status, date)
    }

    List<WordStatusDate> getCurrentRecords() {
        List<WordStatusDate> currentRecords = new ArrayList<>()

        List<Path> currentPaths = getCurrentPaths(root)

        for (Path path : currentPaths) {
            WordStatusDate record = readRecord(path)
            currentRecords.add(record)
        }

        // Sort all records in chronological order.
        currentRecords.sort()

        return currentRecords
    }

    static class WordStatusDate implements Comparable<WordStatusDate>
    {
        String word
        String status
        Date date

        static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        static SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");

        WordStatusDate(String word, String status, String date) {
            this.word = word
            this.status = status
            this.date = format.parse(date)
        }

        String getWord() {
            return word
        }

        String getStatus() {
            return status
        }

        Date getDate() {
            return date
        }

        Date getDay() {
            Calendar cal = Calendar.getInstance(); // locale-specific
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTime();
        }

        @Override
        String toString() {
            return "${format.format(date)}\t$word\t$status"
        }

        @Override
        int compareTo(WordStatusDate that) {
            return this.date.compareTo(that.date)
        }
    }

    static void main(String[] args) {
        String wordsPath = "/Users/eduard/Develop/WordsFiles/status/IT"
        if (args.size() == 1) {
            wordsPath = args[0]
        }
        Path wordsFolder = Paths.get(wordsPath)

        WordStatisticsCrawler wordStatisticsCrawler = new WordStatisticsCrawler(wordsFolder)
        //wordStatisticsCrawler.crawl() // This is just commented out but still very interesting.

        //wordStatisticsCrawler.printLaTeXOfRecords() // This is just commented out but still very interesting.
        //wordStatisticsCrawler.printNanoSyntaxHighlighting()
        wordStatisticsCrawler.printSublimeHightlighting()
    }
}
